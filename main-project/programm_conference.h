#ifndef PROGRAMM_CONFERENCE_H
#define PROGRAMM_CONFERENCE_H

#include "constants.h"

struct time
{
	int hours;
	int minutes;

};

struct person
{
	char first_name[MAX_STRING_SIZE];
	char middle_name[MAX_STRING_SIZE];
	char last_name[MAX_STRING_SIZE];
};

struct programm_conference
{
	person reader;
	time start;
	time finish;
	person author;
	char title[MAX_STRING_SIZE];
};

#endif