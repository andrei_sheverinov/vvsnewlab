#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

time convert(char* str)
{
	time result;
	char* context = NULL;
	char* str_number = strtok_s(str, ":", &context);
	result.hours = atoi(str_number);

	result.minutes = atoi(str_number);

	return result;
}

void read(const char* file_name, programm_conference* array[], int& size)
{
	std::ifstream file(file_name);
	if (file.is_open())
	{
		size = 0;
		char tmp_buffer[MAX_STRING_SIZE];
		while (!file.eof())
		{
			programm_conference* item = new programm_conference;
			file >> tmp_buffer;
			item->start = convert(tmp_buffer);
			file >> tmp_buffer;
			item->finish = convert(tmp_buffer);
			file >> item->reader.last_name;
			file >> item->reader.first_name;
			file >> item->reader.middle_name;
			file.read(tmp_buffer, 1); // ������ ������� ������� �������
			file.getline(item->title, MAX_STRING_SIZE);
			array[size++] = item;
		}
		file.close();
	}
	else
	{
		throw "������ �������� �����";
	}
}