#ifndef FILE_READER_H
#define FILE_READER_H

#include "programm_conference.h"

void read(const char* file_name, programm_conference* array[], int& size);

#endif